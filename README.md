# Tema Geliba para Pelican, generador de sitios estaticos de Python 
## Uno de los pocos temas de Pelican que pueden encontrarse (casi) completamente en español para este generador de sitios estáticos. 


## ¿De qué se trata?

Es un tema creado desde cero para el generador de sitios estáticos Pelican. Pero como nada nace de la nada, claro está, basé mis configuraciones en los temas [brutalist](https://github.com/mc-buckets/brutalist/tree/de551620221ec3f1958250adfaffbbc81e9b748c) y [bootlex](https://github.com/getpelican/pelican-themes/tree/master/bootlex), los cuales se encuentran en el repositorio oficial de Pelican Themes.

## ¿Qué es Pelican y cómo lo uso?

Pelican, como bien dice el título de este README, es un generador de sitios estáticos programado en Python. La idea de éste es la de simplificar la hechura completa de un sitio web, prescindiendo de bases de datos y volviendo a la nimiedad del contenido de antaño, favoreciendo de esta forma el poder subir nuestra web en casi cualquier servidor gratuito y si vamos más allá, plantando bandera ante la obsolescencia programada y proponiendo (y permitiendo) con este contenido poder autgestionarlo en un equipo viejo que actúe como servidor, por citar una de las tantas posibilidades.
La guía y documentación completa se encuentran [acá](https://getpelican.com/). En este apartado vamos a hacer una breve referencia.

* Tenemos que tener instalado Python en nuestro sistema operativo (preferimos Software Libre GNU/Linux)

``` 
sudo apt install python3 python3-pip python3-venv

```

*  Creamos un entorno aislado donde realizar nuestra instalación y primeras pruebas (una carpeta donde volcaremos nuestros bibliotecas Python que no comprometa el entorno global). Línea a línea ejecutamos y presionamos enter en la consola:

``` 
mkdir mistioPelican
cd mistioPelican
python3 -m venv envGeliba
source envGeliba/bin/activate
pip install "pelican[markdown]"
pelican-quickstart

```

* Respondemos a las preguntas de la última instancia y ya podemos empezar a escribir. Pero... ¿dónde? En la carperta ***content***. Allí guardaremos todo el contenido creado a mano con la extensión .md (Markdown). Markdown es un lenguaje de escritura muy simple que simplifica el hecho de tener que andar escribiendo en Html, que puede llegar a ser un tanto confuso para el neófito. Por ejemplo, esto está escrito en Markdown, y en el siguiente apartado también tenés un ejemplo de cómo armar un archivo para publicar.

* Una vez que tenemos el primer contenido creado, volvemos nuevamente a la raíz de *mistioPelican* y tipeamos:

``` 
pelican content
pelican --listen

```

* De este modo podremos ver en nuestro navegador, en el puerto 8000 (localhost:8000) cómo quedaría.


## Sumando el tema GeLiba a nuestro sitio

Pelican tiene la posibilidad de "lavarle la cara" al sitio con diferentes temas y de una manera muy sencilla, a saber. 

* Primero que nada tenemos que clonar este repo:

``` 
git clone https://gitlab.com/enlacepilar/tema-geliba-para-pelican-sitios-estaticos.git

```

* Después, en el raíz nuevamente de *mistioPelican*, abrimos el archivo **pelicanconf.py** y agregamos la siguiente línea al final:

``` 
THEME = "/home/VOS/Documentos/misitioPelican/tema-geliba-para-pelican-sitios-estaticos"

```

La ruta debe corresponderse con el lugar donde clonaste el tema.

* Nuevamente ejecutamos las sentencias:

``` 
pelican content
pelican --listen

```
Y ya vamos a poder ver el tema creado en acción. 


## ¿Qué hay en este tema y qué detalles tengo que tener en cuenta?

* Hay un archivo "base" que contiene los accesos a Boostrap 4, Animatecss y un CSS propio para los colores y las fuentes. El H1 tiene la tipografía fanzine-title y los H2 supercomputer. Tiene un ***include*** a un archivo de barra superior, para que de esta forma se encuentre todo más ordenado. En el pie o ***footer*** está el año corriente y alusión al autor que se encuentre configurado en el Pelicanconf de tu sitio.

* La barra superior tiene un acceso en **images/logo.png**, esto quiere decir que si vos ponés una imagen con ese nombre en esta carpeta, dentro de tu ***content***, vas a tener el logo funcional.

* Siguiendo con la barra superior, las páginas que agregues van a estar representadas como enlaces allí (fijate en las capturas para mejor guía). Después tenés un menú con accesos al archivo, las etiquetas del blog y las categorías. Si no te gusta o no te sirve, lo podés eliminar de la barra y listo. :)

* En la página principal lo que quise hacer es traer todos los artículos escritos con el formato ***card*** de Bootstrap. La imagen del costado la podés traer de la siguiente manera (ejemplo en markdown): 

```

Title: Desarrollando una web a los ponchazos con Flask
Date: 2020-06-12 20:18
Author: Hugovksy
Category: Flask
Slug: web-a-los-ponchazos-flask
Status: published
Imagen: images/fotoNASA-7134.jpg

Arranco esta primera publicación con el proyecto que vengo realizando sobre la web de Ocruxaves que decidí migrar de un [blog](http://hugo-enrique-boulocq.blogspot.com/) hacia [esta](https://ocruxaves.com.ar/) (espero que siga vigente cuando hagas click). La idea inicial era trabajar en Python y no moverme de eso, agregarle una base de datos en Mysql. En el camino descubrí que eso (montar la web) tenía que hacerlo con un Framework enorme que había oído nombrar que se llama Django o con las posiblidades más "ligeras", como son Flask o Bottle. Probé las 2 pero me decanté por la primera, más que nada por la información para poder levantarla (había más).

```

O sea, cuando escribas tu artículo agregá la ruta a tu imagen para que la veas representada en tu página principal, si no el mismo sitio te proveerá una genérica.

* Las fechas de los artículos están configuradas en español.

* La sección ***archivo*** genera un índice, como pude ver en alguna página web, con la fecha y la descripción de la publicación.


***31-01-23***

Agrego un período de archivos. En cada artículo se puede acceder al período correspondiente. Por ejemplo si el artículo es del 2022, haciendo clic en *Ver más publicaciones del año 2022* puedo acceder al listado del año seleccionado exclusivamente. Si quiero ver todo el archivo, tengo que acceder desde la barra superior en Menú **Archivo Completo**. 

Para acceder a estas funciones, en tu pelicanconf.py debés agregar lo siguiente:

```
YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'

ARTICLE_PATHS = ['posts']
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{slug}.html'
ARTICLE_URL = 'posts/{date:%Y}/{slug}.html'

```

Por el momento eso es todo, cualquier cosa me escribís a:

[enlacepilar](mailto:enlacepilar@protonmail.com)

<hr>

Podés ver en funcionamiento el tema en:
[Geliba-web](https://geliba.enlacepilar.com.ar/)









